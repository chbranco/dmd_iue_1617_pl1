//
//  CountryTest.swift
//  PersonTabGit_PL1
//
//  Created by Luis Marcelino on 17/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import XCTest

class CountryTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCountryParsing() {
        let serverAnswer = "{\"geonames\":[{\"continent\":\"EU\",\"capital\":\"Berlino\",\"languages\":\"de\",\"geonameId\":2921044,\"south\":47.275776,\"isoAlpha3\":\"DEU\",\"north\":55.055637,\"fipsCode\":\"GM\",\"population\":\"81802257\",\"east\":15.039889,\"isoNumeric\":\"276\",\"areaInSqKm\":\"357021.0\",\"countryCode\":\"DE\",\"west\":5.865639,\"countryName\":\"Germania\",\"continentName\":\"Europa\",\"currencyCode\":\"EUR\"}]}"
        // data as receiverd from server
        let serverData = serverAnswer.data(using: .utf8)!
        if let jsonDic = try? JSONSerialization.jsonObject(with: serverData, options: .allowFragments) as? [String:Any] {
            if let geonamesArray = jsonDic?["geonames"] as? [[String:Any]] {
                for countryJSON in geonamesArray {
                    let country = Country.parse(json: countryJSON)
                    XCTAssertEqual(country?.name, "Germania")
                    XCTAssertEqual(country?.capital, "Berlino")
                    XCTAssertEqual(country?.population, 81802257)
                }
                
            }
            else {
                XCTFail("Could not convert array")
            }
            
        }
        
        
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    
    func testCountryFetch() {
        
        let expect = expectation(description: "Fetching data from server")
        
        GeonamesClient.fetchCountry(countryCode: "PT") { (country) in
            XCTAssertEqual(country?.name, "Portugal")
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 5) { (error) in
            if error != nil {
                XCTFail("Time out")
            }
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
