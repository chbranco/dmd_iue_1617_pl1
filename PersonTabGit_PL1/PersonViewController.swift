//
//  PersonViewController.swift
//  PersonTabGit_PL1
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import Social

class PersonViewController: UIViewController {

    @IBOutlet weak var labelText: UILabel!
    
    @IBOutlet weak var firstNameText: UITextField!
    
    @IBOutlet weak var lastNameText: UITextField!
    
    @IBOutlet weak var nationalityText: UITextField!
    
    @IBOutlet weak var textToPersist: UITextField!
    
    var person: Person?
    
    let defaults = UserDefaults.standard
    
    override func viewWillDisappear(_ animated: Bool) {
        defaults.set(textToPersist.text, forKey: "text")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        textToPersist.text = defaults.object(forKey: "text") as? String
    }
    
    
    @IBAction func shareButtonPressed(_ sender: UIBarButtonItem) {

        guard let firstName = firstNameText.text else {
            sharingError(message: "Person has no first name")
            print("Person has no first name")
            return
        }
        guard let lastName = lastNameText.text else {
            sharingError(message: "Person has no last name")
            print("Person has no first name")
            return
        }
        
        let viewController = UIActivityViewController(activityItems: [firstName, lastName], applicationActivities: nil)
        
        present(viewController, animated: true, completion: nil)

        
        let actionSheet = UIAlertController(title: "", message: "Share person details", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let twitterAction = UIAlertAction(title: "Share on Twitter", style: .default) { (action) -> Void in
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                if let twitterVC = SLComposeViewController(forServiceType: SLServiceTypeTwitter){
                    //protect case of empty first name
                    twitterVC.setInitialText(self.firstNameText.text)
                self.present(twitterVC, animated: true, completion: nil)
                }
            } else {
                self.sharingError(message: "Twitter must be set on the device.")
            }
        }
        let dismissAction = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        actionSheet.addAction(twitterAction)
        actionSheet.addAction(dismissAction)
        present(actionSheet, animated: true, completion: nil)
    }
    
    func sharingError (message:String) -> Void {
        let alert = UIAlertController(title: "Sharing error", message:
            message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style:
            UIAlertActionStyle.cancel, handler: nil))
        self.present(alert, animated:  true, completion:  nil)
    }
    
    
    
    
    
    @IBAction func popButtonPressed(_ sender: UIButton) {
           dismiss(animated: true, completion: nil)
    }
    @IBAction func saveAction(_ sender: AnyObject) {
        if person == nil {
            self.person = Person(firstName: firstNameText.text!,
                                 lastName: lastNameText.text!,
                                 nationality: nationalityText.text!)
            PeopleRepository.repository.people.append(self.person!)
            
            PeopleRepository.repository.savePeople()
            self.navigationController?.dismiss(animated: true, completion: nil)
        }
        else {
            self.person!.firstName = firstNameText.text
            self.person!.lastName = lastNameText.text
            self.person!.nationality = nationalityText.text!
            PeopleRepository.repository.savePeople()
            
            self.navigationController!.popViewController(animated: true)
            
        }
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.people.append(self.person!)
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.firstNameText.text = self.person?.firstName
        self.lastNameText.text = self.person?.lastName
        self.nationalityText.text = self.person?.nationality
        
        if self.person == nil {
            let cancelBtn = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(PersonViewController.cancelAction))
            self.navigationItem.leftBarButtonItem = cancelBtn
        }
        else {
            self.title = "Edit person"
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func cancelAction() {
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
    
    
}
