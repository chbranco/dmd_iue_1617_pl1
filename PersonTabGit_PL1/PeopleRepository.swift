//
//  PeopleRepository.swift
//  PersonTabGit_PL1
//
//  Created by Catarina Silva on 13/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class PeopleRepository:RepositoryProtocol {
    
    //singleton repository
    static let repository = PeopleRepository() //shared instance
    init() {}
    
    var documentsPath:URL = URL(fileURLWithPath: "")
    var filePath:URL = URL(fileURLWithPath: "")
    
    var people = [Person]()
    
    func savePeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])

        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("People.data", isDirectory: false)
        print(filePath.path)
        
        if NSKeyedArchiver.archiveRootObject(people, toFile: filePath.path) {
            print("Successfully saved all people!!!!!!!!!")
        }
        else {
            print("Failure saving people.")
        }
    }
    func loadPeople() {
        documentsPath = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])
        
        print(documentsPath)
        filePath = documentsPath.appendingPathComponent("People.data", isDirectory: false)
        print(filePath.path)
        if let newData = NSKeyedUnarchiver.unarchiveObject(withFile: filePath.path) as? [Person]{
            people = newData
        }
    }
}

