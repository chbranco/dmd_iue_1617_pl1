//
//  PersonTableViewCell.swift
//  PersonTabGit_PL1
//
//  Created by Luis Marcelino on 07/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit

class PersonTableViewCell: UITableViewCell {

    @IBOutlet weak var firstNameLabel: UILabel!
    
    @IBOutlet weak var lastNameLabel: UILabel!
    
    @IBOutlet weak var nationalityLabel: UILabel!
    
    var person: Person? {
        didSet {
            self.firstNameLabel.text = self.person?.firstName
            self.lastNameLabel.text = self.person?.lastName
            self.nationalityLabel.text = self.person?.nationality
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
