//
//  Person.swift
//  PersonTabGit_PL1
//
//  Created by Catarina Silva on 29/09/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class Person:NSObject, NSCoding {
    var firstName:String?
    var lastName:String?
    var nationality:String
    
//    init() {
//        self.nationality = "Portuguese"
//    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(nationality, forKey: "nationality")
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        firstName = aDecoder.decodeObject(forKey: "firstName") as! String?
        lastName = aDecoder.decodeObject(forKey: "lastName") as! String?
        let nat = aDecoder.decodeObject(forKey: "nationality") as! String?
        if let n = nat {
            nationality = n
        } else {
            nationality = "Portuguese"
        }
    }

    init(firstName:String, lastName:String, nationality: String) {
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    func fullName() -> String {
        return firstName! + " " + lastName!
    }
}
