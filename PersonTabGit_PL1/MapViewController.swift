//
//  MapViewController.swift
//  PersonTabGit_PL1
//
//  Created by Catarina Silva on 06/10/16.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class MapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    let locationManager = CLLocationManager()
    
    var annotation = MKPointAnnotation()
    
    var location:CLLocationCoordinate2D?
    
    @IBOutlet weak var map: MKMapView!
    
    
    

    @IBAction func didTapMap(_ sender: UITapGestureRecognizer) {
        
        let tapPoint = sender.location(in: map)
        
        location = map.convert(tapPoint, toCoordinateFrom: map)
        
    
        map.removeAnnotation(annotation)
        annotation.coordinate = location!
        annotation.title = "tapped position"
        map.addAnnotation(annotation)
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.distanceFilter = kCLDistanceFilterNone
        
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        else {
            //inform user
        }
        
        map.delegate = self
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Location error: \(error)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        //if status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        //}
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let locationObj = locations.last
        let coord = locationObj?.coordinate
        if let c = coord {
            print("latitude: \(c.latitude)  longitude: \(c.longitude)")
            map.setRegion(MKCoordinateRegionMake(c, MKCoordinateSpanMake(0.1, 0.1)), animated: true)
            
            annotation.coordinate = c
            annotation.title = "Current Position"
            annotation.subtitle = "Here"
            
            map.addAnnotation(annotation)
            
        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
