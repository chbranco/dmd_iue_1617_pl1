//
//  Country+JSON.swift
//  PersonTabGit_PL1
//
//  Created by Luis Marcelino on 17/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

extension Country {
    
    static func parse(json:[String:Any]) -> Country? {
        guard let name = json["countryName"] as? String else {
            return nil
        }
        guard let populationString = json["population"] as? String else {
            return nil
        }
        guard let population = Int(populationString) else {
            return nil
        }
        guard let capital = json["capital"] as? String else {
            return nil
        }
    
        let country = Country(name: name, population: population, capital: capital)
        return country
    }
}
