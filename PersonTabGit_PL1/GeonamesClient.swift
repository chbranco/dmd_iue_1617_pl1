//
//  GeonamesClient.swift
//  PersonTabGit_PL1
//
//  Created by Luis Marcelino on 28/10/2016.
//  Copyright © 2016 Ipleiria. All rights reserved.
//

import Foundation

class GeonamesClient {
    // http://api.geonames.org/countryInfoJSON?lang=en&country=DE&username=desenvolvimentoswift
    
    static func fetchCountry (countryCode: String, completion: @escaping (Country?) -> Void ) {
        if let url = URL(string: "http://api.geonames.org/countryInfoJSON?lang=en&country=\(countryCode)&username=desenvolvimentoswift") {
            let dataTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
                if data == nil {
                    completion(nil);
                    return
                }
                // we know that data != nil
                if let jsonDic = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? [String:Any] {
                    if let geonamesArray = jsonDic?["geonames"] as? [[String:Any]] {
                        for countryJSON in geonamesArray {
                            let country = Country.parse(json: countryJSON)
                            completion(country)
                            return
                        }
                    }
                    completion(nil)
                }
                
            })
            dataTask.resume()
            return
        }
        completion(nil)
    }
}
